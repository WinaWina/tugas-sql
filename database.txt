Setting environment for using XAMPP for Windows.
user@WINA c:\xampp
# mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.24-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

SOAL 1 & 2

MariaDB [(none)]> create database myshop
    -> ;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.015 sec)

MariaDB [(none)]> use myshop;
Database changed
MariaDB [myshop]> create table users
    -> (id int unsigned auto_increment primary key,
    -> name varchar(255) not null,
    -> email varchar(255) not null,
    -> password varchar(255) not null);
Query OK, 0 rows affected (0.019 sec)
MariaDB [myshop]> create table categories
    -> (id int unsigned auto_increment primary key,
    -> name varchar(255) not null);
Query OK, 0 rows affected (0.023 sec)
MariaDB [myshop]> create table items
    -> (id int unsigned auto_increment primary key,
    -> name varchar(255) not null,
    -> description varchar(255) not null,
    -> price int not null,
    -> stock int not null,
    -> category_id int not null,
    -> foreign key (id) references categories(id)
    -> on delete cascade on update cascade);
Query OK, 0 rows affected (0.024 sec)
Query OK, 0 rows affected (0.023 sec)

SOAL 3

MariaDB [myshop]> insert into users
    -> (name, email, password)
    -> value
    -> ("John Doe","john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.047 sec)
Records: 2  Duplicates: 0  Warnings: 0
MariaDB [myshop]> insert into categories
    -> (name)
    -> value
    -> ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");
Query OK, 5 rows affected (0.004 sec)
Records: 5  Duplicates: 0  Warnings: 0
MariaDB [myshop]> insert into items
    -> (name, description, price, stock, category_id)
    -> value
    -> ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", "1"),
    -> ("Uniklooh", "baju keren dari brand ternama", "500000", "50", "2"),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000","10", "1");
Query OK, 3 rows affected (0.005 sec)
Records: 3  Duplicates: 0  Warnings: 0

SOAL 4

MariaDB [myshop]> select name, email from users;
+----------+--------------+
| name     | email        |
+----------+--------------+
| John Doe | john@doe.com |
| Jane Doe | jane@doe.com |
+----------+--------------+
2 rows in set (0.000 sec)

MariaDB [myshop]> select price
    -> from items
    -> where price > 1000000;
+---------+
| price   |
+---------+
| 4000000 |
| 2000000 |
+---------+
2 rows in set (0.000 sec)

MariaDB [myshop]> select *from items
    -> where name like '%Watch';
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | description                       | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
1 row in set (0.001 sec)
MariaDB [myshop]> select name
    -> from items
    -> where name REGEXP "Watch$";
+------------+
| name       |
+------------+
| IMHO Watch |
+------------+
1 row in set (0.001 sec)
MariaDB [myshop]> select *from items a inner join
    -> categories b
    -> on a. category_id = b.id;
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
| id | name        | description                       | price   | stock | category_id | id | name   |
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |  1 | gadget |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |  2 | cloth  |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |  1 | gadget |
+----+-------------+-----------------------------------+---------+-------+-------------+----+--------+
3 rows in set (0.001 sec)
MariaDB [myshop]> update items
    -> set price = 2500000
    -> where id = 1;
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

SOAL 5

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)
